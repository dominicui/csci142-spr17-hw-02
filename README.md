# README #

owner: Dominic

assignment: What I learnt from example?

commit for starting

Assertions:
1.  Square: sideLength = -5.0  area = 25.0
    Mathematically right answer, but sideLength could not be negtive. SideLength should be checked for valid.
2.  Square: sideLength = 5.0  area = 25.0
3.  null: sideLength = 5.0  area = 25.0
    Based on <assert myType != null : "type must be set";>, When we set type, we can get the message we want. 
    One thing I do not understand. if we does not set type, run as null. Should not print "type must be set" rather than "null"?
4.  Exception in thread "main" java.lang.AssertionError: side length must be > 0
    This is a way to solve first problem.
5.  We should care details, otherwise we may mot get what we want after run the project.

Polymorphism:
	<System.out.println((shapes[1]).getSideLength());>
	<System.out.println((shapes[2]).getWidth());>
        getSideLength and getWidth must be set in interface shape. After adding them in interface shape, project can run, 
        even though there are still exists errors. The problem is that both rectangle and square are inherited shape, 
        but they are not both have getSideLength and getWidth at same time. By solving this problem, there are two options 
        which are add unimplemented method or make type square abstract. It is better choose first way. Under second way, we 
        still have to change other code in main.

        write a loop for print:
        Figures.java
        for(int i = 0; i <= 3; i++)
        {
            System.out.println((shapes[i]).getArea());
       	    System.out.println((shapes[i]).getSideLength());
	}
        Rectangle.java
        @Override
	public float getSideLength() 
        {
	    return myWidth;
	}

continuing later or tomorrow

Alias and cloning:
1.  Square: sideLength = 5.0  area = 25.0  upper left point = java.awt.Point[x=11,y=12]
    Square: sideLength = 5.0  area = 25.0  upper left point = java.awt.Point[x=11,y=12]
2.  Square: sideLength = 3.0  area = 9.0  upper left point = java.awt.Point[x=11,y=12]
    Square: sideLength = 5.0  area = 25.0  upper left point = java.awt.Point[x=2,y=2]
    
    clone copying data, but both object are individual. Equal makes two bond together; 
one be changed, another also change. 
3.  comparison
4.  Square: sideLength = 3.0  area = 9.0  upper left point = java.awt.Point[x=11,y=12]
    Square: sideLength = 5.0  area = 25.0  upper left point = java.awt.Point[x=2,y=2]
    
    Square2 clone Square1's data and change sidelength later. So Square2 has Square1's 
upperLeftPoint location, (2,2), and new sideLength which reseted to 5.0.
    Point.x and Point.y be updated. Whatever <square1.setUpperLeftPoint(point);> commented or uncommented, square1 would has new location. Square2's location would not be changed
because point be reset after clone.